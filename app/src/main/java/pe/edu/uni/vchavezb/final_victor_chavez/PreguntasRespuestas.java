package pe.edu.uni.vchavezb.final_victor_chavez;

public class PreguntasRespuestas {
    public static String[] Preguntas ={
            "CO2 es dióxido de carbono.?",
            "O3 es oxígeno?",
            "NaCl es cloruro de sodio?",
            "Fe2O3 es óxido de hierro?",
            "Mg2O es óxido de magnesio?",
            "La capital de Corea del Norte es Seúl.",

    };
    public static String[][] choices ={
            {"Verdadero" ,"Falso"},
            {"Verdadero" ,"Falso"},
            {"Verdadero" ,"Falso"},
            {"Verdadero" ,"Falso"},
            {"Verdadero" ,"Falso"},
            {"Verdadero" ,"Falso"},

    };
    public static String[] Correctas ={
            "Verdadero",
            "Falso",
            "Verdadero",
            "Verdadero",
            "Falso",
            "Falso"


    };
}
