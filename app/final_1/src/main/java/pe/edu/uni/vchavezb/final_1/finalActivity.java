package pe.edu.uni.vchavezb.final_1;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class finalActivity extends AppCompatActivity implements View.OnClickListener{
    TextView total_preguntas;
    Button verdadero;
    Button falso;
    Button siguiente;
    Button anterior;

    int total=0;
    int index=0;
    String seleccionar ="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final);
        total_preguntas=findViewById(R.id.total_preguntas);
        verdadero =findViewById(R.id.verdadero);
        falso =findViewById(R.id.falso);
        siguiente =findViewById(R.id.siguiente);
        anterior =findViewById(R.id.anterior);

        verdadero.setOnClickListener(this);
        falso.setOnClickListener(this);
        siguiente.setOnClickListener(this);
        anterior.setOnClickListener(this);

    }



    @Override
    public void onClick(View v) {

    }
}